#!/usr/bin/env bash

set -eux

main() {
  local version=$1
  local container=$2
  cd "containers/${version}/${container}"
  grep -i '^from' Dockerfile | grep -vP '(scratch)' | cut -d ' ' -f2 | sort -u | xargs -I {} docker pull {}
  docker build -t "registry.gitlab.com/vito-containers/koel/${container}:${version}" .
}

main "$@"
