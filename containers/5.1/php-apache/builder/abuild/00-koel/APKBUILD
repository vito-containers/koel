# Contributor: Vitoyucepi <vitoyucepi@cock.li>
# Maintainer: Vitoyucepi <vitoyucepi@cock.li>
# shellcheck disable=SC2034,SC2154
pkgname=koel
pkgver=5.1.0
_pkgver="v$pkgver"
pkgrel=99
pkgdesc='A personal music streaming server'
url='https://koel.dev/'
arch='all'
license='MIT'
depends=''
makedepends=''
subpackages=''
source="https://github.com/koel/koel/releases/download/$_pkgver/koel-$_pkgver.tar.gz"
options='!check'
pkgusers='www-data'
pkggroups='www-data'
builddir="$srcdir/$pkgname"

package() {
  # Remove git components
  find "$srcdir" -name .gitmodules -type f -delete
  find "$srcdir" -name .gitignore -type f -delete
  find "$srcdir" -name .gitkeep -type f -delete
  find "$srcdir" -name .gitattributes -type f -delete

  # Remove tests
  find "$srcdir" -name cypress -type d -exec rm -r {} +
  find "$srcdir" -name cypress.json -type f -delete
  find "$srcdir" -name __tests__ -type d -exec rm -r {} +
  find "$srcdir" -name tests -type d -exec rm -r {} +

  # Remove docs
  find "$srcdir" -name 'LICENSE*' -type f -delete
  find "$srcdir" -name 'README*' -type f -delete
  find "$srcdir" -name 'DOCKER_README.MD' -type f -delete
  find "$srcdir" -name 'readme*' -type f -delete
  find "$srcdir" -name 'CHANGELOG*' -type f -delete
  find "$srcdir" -name 'CHANGES*' -type f -delete
  find "$srcdir" -name 'changelog*' -type f -delete
  find "$srcdir" -name 'CONTRIBUTING*' -type f -delete
  find "$srcdir" -name 'contributing*' -type f -delete
  find "$srcdir" -name 'UPGRADE*' -type f -delete
  find "$srcdir" -name 'UPGRADING*' -type f -delete
  find "$srcdir" -name 'CODE_OF_CONDUCT*' -type f -delete
  find "$srcdir" -name 'CONDUCT*' -type f -delete
  find "$srcdir" -name 'SECURITY*' -type f -delete
  find "$srcdir" -name 'NOTICE*' -type f -delete
  rm -r "$builddir/api-docs"

  # Remove 3rd-party tools configs
  find "$srcdir" -name .github -type d -exec rm -r {} +
  find "$srcdir" -name .circleci -type d -exec rm -r {} +
  find "$srcdir" -name '.gitpod.*' -type f -delete
  find "$srcdir" -name .travis.yml -type f -delete
  find "$srcdir" -name .scrutinizer.yml -type f -delete
  find "$srcdir" -name appveyor.yml -type f -delete
  find "$srcdir" -name .editorconfig -type f -delete
  find "$srcdir" -name .eslintrc -type f -delete
  find "$srcdir" -name .eslintignore -type f -delete
  find "$srcdir" -name Dockerfile -type f -delete
  find "$srcdir" -name .dockerignore -type f -delete
  find "$srcdir" -name Makefile -type f -delete
  find "$srcdir" -name 'phpstan*' -type f -delete
  find "$srcdir" -name '.php_cs.*' -type f -delete
  find "$srcdir" -name 'php_cs.*' -type f -delete
  find "$srcdir" -name 'phpcs.*' -type f -delete
  find "$srcdir" -name 'phpunit.*' -type f -delete
  find "$srcdir" -name 'psalm.*' -type f -delete
  find "$srcdir" -name 'ruleset.xml' -type f -delete

  #Remove examples
  rm "$builddir/.env.example"
  rm "$builddir/nginx.conf.example"
  rm "$builddir/public/manifest.json.example"
  rm "$builddir/public/manifest-remote.json.example"
  rm "$builddir/public/.user.ini.example"

  #Remove lock files
  rm "$builddir/composer.lock"
  rm "$builddir/yarn.lock"

  #Remove various
  rm "$builddir/webpack.config.js"
  rm "$builddir/webpack.mix.js"
  rm "$builddir/tag.sh"

  install -dm750 "$pkgdir/var/www/html"
  cp -r "$srcdir/$pkgname/" "$pkgdir/var/www/html"
  install -dm750 "$pkgdir/var/www/html/storage/search-indexes"
  chown www-data:www-data . -R "$pkgdir/var/www/html"
}
sha512sums="09ebea34ccf3355d19d57149a4bc9d0b16c324a9442dd85df52dbe0ce6977b6c2ff951cde28f0ef2999a6093658856d23b59222ced24e2544021fdd3ae495153  koel-v5.1.0.tar.gz"
