<?php

declare(strict_types=1);

/**
 * @SuppressWarnings(PHPMD.ExitExpression)
 * @param int $signalNumber
 */
function signalHandler(int $signalNumber = 0): void
{
    switch ($signalNumber) {
        case SIGQUIT:
        case SIGWINCH:
        case SIGTERM:
        case SIGINT:
            /** @noinspection ForgottenDebugOutputInspection */
            error_log("Got signal $signalNumber");
            exit(0);
    }
}

function initSignals()
{
    foreach ([SIGTERM, SIGINT, SIGQUIT, SIGWINCH] as $signal) {
        pcntl_signal($signal, 'signalHandler');
    }
}

function logMessage(string $errorMessage, string $prefix = 'Error', ?int $errorCode = null): void
{
    if ($errorCode === null) {
        $errorMessage = sprintf('%s: %s', $prefix, $errorMessage);
    } else {
        $errorMessage = sprintf('%s: (%d) %s', $prefix, $errorCode, $errorMessage);
    }
    /** @noinspection ForgottenDebugOutputInspection */
    error_log($errorMessage);
}

/**
 * @param string $envVar
 * @param string|bool|null $defaultValue
 * @return string|bool|null
 */
function customGetEnv(string $envVar, $defaultValue = null)
{
    $retVal = getenv($envVar);
    if ($retVal === false || is_array($retVal)) {
        return $defaultValue;
    }
    return $retVal;
}

function connectDatabase(): PDO
{
    $databaseType = customGetEnv('DB_CONNECTION', 'mysql');
    $dataSourceName = '';
    $databaseUser = '';
    $databasePass = '';
    $connectionOptions = [PDO::ATTR_TIMEOUT => 5];

    switch ($databaseType) {
        case 'pgsql':
            $databaseName = customGetEnv('DB_DATABASE', 'koel');
            $databaseHost = customGetEnv('DB_HOST', 'db');
            $databaseUser = customGetEnv('DB_USERNAME', 'koel');
            $databasePass = customGetEnv('DB_PASSWORD', 'koel');
            $databasePort = (int)customGetEnv('DB_PORT', '5432');
            $dataSourceName = sprintf('pgsql:dbname=%s;host=%s;port=%d', $databaseName, $databaseHost, $databasePort);
            break;
        case 'mysql':
            $databaseName = customGetEnv('DB_DATABASE', 'koel');
            $databaseHost = customGetEnv('DB_HOST', 'db');
            $databaseUser = customGetEnv('DB_USERNAME', 'koel');
            $databasePass = customGetEnv('DB_PASSWORD', 'koel');
            $databasePort = (int)customGetEnv('DB_PORT', '3306');
            $dataSourceName = sprintf('mysql:dbname=%s;host=%s;port=%d', $databaseName, $databaseHost, $databasePort);
            break;
        case 'sqlsrv':
            $databaseName = customGetEnv('DB_DATABASE', 'koel');
            $databaseHost = customGetEnv('DB_HOST', 'db');
            $databaseUser = customGetEnv('DB_USERNAME', 'koel');
            $databasePass = customGetEnv('DB_PASSWORD', 'koel');
            $databasePort = (int)customGetEnv('DB_PORT', '1433');
            $dataSourceName = sprintf('sqlsrv:dbname=%s;host=%s;port=%d', $databaseName, $databaseHost, $databasePort);
            break;
        case 'sqlite-persistent':
            $databaseName = customGetEnv('DB_DATABASE', '/var/www/html/database/koel.sqlite');
            $dataSourceName = sprintf('sqlite:%s', $databaseName);
            break;
    }
    return new PDO($dataSourceName, $databaseUser, $databasePass, $connectionOptions);
}
