<?php

declare(strict_types=1);

require_once __DIR__ . '/functions.php';

function isFatalDatabaseError(?string $errorCode, ?string $errorMessage): bool
{
    if ($errorCode === '08006') {
        if (strpos($errorMessage, 'Connection refused') !== false) {
            return false;
        }
        if (strpos($errorMessage, 'Name or service not known') !== false) {
            return false;
        }
        if (strpos($errorMessage, 'timeout expired') !== false) {
            return false;
        }
        if (strpos($errorMessage, 'the database system is starting up') !== false) {
            return false;
        }
    } elseif ($errorCode === 'HY000') {
        if (strpos($errorMessage, 'Connection refused') !== false) {
            return false;
        }
    }
    return true;
}

function checkDatabase(): int
{
    $errorPrefix = 'Failed to connect to database';
    try {
        connectDatabase();
        logMessage('Database is available', 'Notice');
    } catch (PDOException $exception) {
        [$errorCode, , $errorMessage] = $exception->errorInfo;
        logMessage($exception->getMessage(), $errorPrefix);
        if (isFatalDatabaseError($errorCode, $errorMessage)) {
            return 1;
        }
        return -1;
    }
    return 0;
}

function calculateResult(int ...$checkResults): int
{
    $returnResult = 0;
    foreach ($checkResults as $checkResult) {
        if ($checkResult > 0) {
            return $checkResult;
        }
        if ($checkResult === -1) {
            $returnResult = -1;
        }
    }
    return $returnResult;
}

function main(): int
{
    initSignals();
    $checkResult = -1;
    while ($checkResult === -1) {
        $checks = [checkDatabase()];
        $checkResult = calculateResult(...$checks);
        sleep(1);
    }
    return $checkResult;
}

/** @SuppressWarnings(PHPMD.ExitExpression) */
exit(main());
